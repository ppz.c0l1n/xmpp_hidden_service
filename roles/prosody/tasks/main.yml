- name: "Install Prosody"
  apt: name=prosody state=present
- name: "Install Lua zLib"
  apt: name=lua-zlib state=present
- name: "Install Lua DBI sqlite"
  apt: name=lua-dbi-sqlite3 state=present
- name: "Install sqlite3"
  apt: name=sqlite3 state=present
- name: "Install lua-bit32"
  apt: name=lua-bit32 state=present
  when: (tor == "true") and (ansible_distribution != "Debian")

# Workaround for Debian Stable (Use sid lua-bit32)
- name: "Add sid repo"
  lineinfile: dest=/etc/apt/sources.list line="deb http://mirrors.kernel.org/debian/ sid main"
  when: (tor == "true") and (ansible_distribution == "Debian")

# Workaround for Debian Stable (Use sid lua-bit32)
- name: "Set default release to stable (don't use sid except for this package)"
  lineinfile: dest=/etc/apt/apt.conf.d/70debconf line='APT::Default-Release "stable";'
  when: (tor == "true") and (ansible_distribution == "Debian")

# Workaround for Debian Stable (Use sid lua-bit32)
- name: "Update apt cache"
  apt: update_cache=yes
  when: (tor == "true") and (ansible_distribution == "Debian")

# Workaround for Debian Stable (Use sid lua-bit32)
- name: "Install lua-bit32"
  apt: name=lua-bit32 state=present default-release=sid
  when: (tor == "true") and (ansible_distribution == "Debian")

- name: "Get tor hostname"
  shell: cat /var/lib/tor/prosody/hostname
  register: tor_hostname
  changed_when: false
  when: tor == "true"

- name: "Remove snakeoil cert"
  file: name=/etc/prosody/certs/localhost.cert state=absent
- name: "Remove snakeoil key"
  file: name=/etc/prosody/certs/localhost.key state=absent

- name: "Generate new key and cert (non-tor)"
  shell: openssl req -x509 -newkey rsa:4096 -keyout /etc/prosody/certs/{{ prosody_hostname }}.key -out /etc/prosody/certs/{{ prosody_hostname }}.pem -days 3650 -nodes -subj '/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN={{ prosody_hostname }}' creates=/etc/prosody/certs/{{ prosody_hostname }}.key
  notify: Restart prosody
  when: not tor_hostname

- name: "Generate new key and cert (tor)"
  shell: openssl req -x509 -newkey rsa:4096 -keyout /etc/prosody/certs/{{ tor_hostname.stdout }}.key -out /etc/prosody/certs/{{ tor_hostname.stdout }}.pem -days 3650 -nodes -subj '/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN={{ tor_hostname.stdout }}' creates=/etc/prosody/certs/{{ tor_hostname.stdout }}.key
  notify: Restart prosody
  when: tor_hostname

- name: "Push new prosody config"
  template: src=../templates/prosody.cfg.lua.j2 dest=/etc/prosody/prosody.cfg.lua
  notify: Restart prosody

- name: "Push mod_blocking.lua to prosody modules"
  copy: src=../files/mod_blocking.lua dest=/usr/lib/prosody/modules/mod_blocking.lua
  notify: Restart prosody
- name: "Push mod_carbons.lua to prosody modules"
  copy: src=../files/mod_carbons.lua dest=/usr/lib/prosody/modules/mod_carbons.lua
  notify: Restart prosody
- name: "Push mod_csi.lua to prosody modules"
  copy: src=../files/mod_csi.lua dest=/usr/lib/prosody/modules/mod_csi.lua
  notify: Restart prosody
- name: "Push mod_http_upload.lua to prosody modules"
  copy: src=../files/mod_http_upload.lua dest=/usr/lib/prosody/modules/mod_http_upload.lua
  notify: Restart prosody
- name: "Push mod_smacks.lua to prosody modules"
  copy: src=../files/mod_smacks.lua dest=/usr/lib/prosody/modules/mod_smacks.lua
  notify: Restart prosody
- name: "Make MAM directory"
  file: name=/usr/lib/prosody/modules/mod_mam/ state=directory
- name: "Push mod_onions.lua to prosody modules"
  copy: src=../files/mod_onions.lua dest=/usr/lib/prosody/modules/mod_onions.lua
  notify: Restart prosody
- name: "Push mod_mam.lua to prosody modules"
  copy: src=../files/mod_mam.lua dest=/usr/lib/prosody/modules/mod_mam.lua
  notify: Restart prosody
- name: "Push fallback_archive.lib.lua to prosody modules"
  copy: src=../files/fallback_archive.lib.lua dest=/usr/lib/prosody/modules/mod_mam/fallback_archive.lib.lua
  notify: Restart prosody
- name: "Push mamprefs.lib.lua to prosody modules"
  copy: src=../files/mamprefs.lib.lua dest=/usr/lib/prosody/modules/mod_mam/mamprefs.lib.lua
  notify: Restart prosody
- name: "Push mamprefsxml.lib.lua to prosody modules"
  copy: src=../files/mamprefsxml.lib.lua dest=/usr/lib/prosody/modules/mod_mam/mamprefsxml.lib.lua
  notify: Restart prosody
- name: "Push rsm.lib.lua to prosody modules"
  copy: src=../files/rsm.lib.lua dest=/usr/lib/prosody/modules/mod_mam/rsm.lib.lua
  notify: Restart prosody

- name: "Print tor hostname"
  debug: msg="Tor hostname is {{tor_hostname.stdout}}"
  when: tor == "true"
