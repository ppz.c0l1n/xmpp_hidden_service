# Ansible Roles For: XMPP Hidden Service

These Ansible roles and the playbook will allow you to spawn a Tor Hidden
Service running Prosody XMPP. It has been tested on Debian Stable (although it
will add Debian Unstable sources for one needed package) and Ubuntu 16.04.

For full details (**AND SOME IMPORTANT SECURITY CAVEATS**), please check out [this blog post](https://samurailink3.com/blog/2016/11/12/tor-hidden-xmpp-server/).
